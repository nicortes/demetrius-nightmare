﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
    private ScoreController scoreController;

    public void LoadNextSceneIn(float seconds) {
        Invoke("LoadNextScene", seconds);
    }

    public void LoadNextScene() {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentSceneIndex + 1);
    }

    public void Reset() {
        ResetScore();
        LoadStartScene();
    }

    private void ResetScore() {
        scoreController = FindObjectOfType<ScoreController>();
        scoreController.ResetScore();
    }

    private void LoadStartScene() {
        SceneManager.LoadScene(0);
    }

    public void QuitGame() {
        Application.Quit();
    }
}