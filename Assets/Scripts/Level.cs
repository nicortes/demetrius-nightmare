﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Level : MonoBehaviour
{
    private int totalVasesInTheLevel;
    private int vasesLeftInTheLevel;

    public int vasesLeft => vasesLeftInTheLevel;
    SceneController sceneController;

    private void Awake() {
        //CountTotalVasesInTheLevel();
        totalVasesInTheLevel = 0;
        vasesLeftInTheLevel = 0;
        sceneController = FindObjectOfType<SceneController>();
    }
    
    private void CheckVasesLeft() {
        if (vasesLeftInTheLevel <= 0) {
            sceneController.LoadNextSceneIn(2.0f);
        }
    }
    
    private void CountTotalVasesInTheLevel() {
        totalVasesInTheLevel = FindObjectsOfType<Vase>().Length;
        vasesLeftInTheLevel = totalVasesInTheLevel;
    }

    public void DiscountBreakableVase() {
        vasesLeftInTheLevel--;
        CheckVasesLeft();
    }

    public void CountBreakableVase() {
        totalVasesInTheLevel++;
        vasesLeftInTheLevel = totalVasesInTheLevel;
    }
}
