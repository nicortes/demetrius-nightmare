﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class ScoreController : Singleton<ScoreController>
{
    [SerializeField]
    private int pointsPerVase = 10;
    [SerializeField]
    private TextMeshProUGUI scoreNumberText;
    [SerializeField]
    private string firstScene = "Main Menu";

    private static int score;
    
    private void Start() {
        InitializeScore();
    }
    
    private void InitializeScore() {
        score = 0;
        SetScoreNumberText();
    }

    private void SetScoreNumberText() {
        scoreNumberText.SetText(score.ToString());
    }
    
    public void increaseScore() {
        score += pointsPerVase;
        SetScoreNumberText();
    }

    public void ResetScore() {
        score = 0;
        SetScoreNumberText();
    }
}
