﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle : MonoBehaviour
{
    [SerializeField] float screenWidthInUnits;
    [SerializeField] Camera camera;

    [SerializeField] float min = 0;
    [SerializeField] float max = 16;

    float cameraOffset;
    float paddleWidth;
    
    void Awake()
    {
        Transform cameraTransform = camera.GetComponent<Transform>();
        float cameraXPosition = cameraTransform.position.x;
        float cameraSize = camera.orthographicSize;
        float cameraHeight = cameraSize * 2;
        float cameraWidth = screenWidthInUnits / 2;
        //  9               17                  8
        cameraOffset = cameraXPosition - cameraWidth;

        //paddleWidth = GetComponent<BoxCollider2D>().size.x;
        paddleWidth = 4.0f;
    }

    // Update is called once per frame
    void Update() {
        //        from 0 to 16              from 0 to 365       365                 16
        float mousePositionInUnits = (Input.mousePosition.x / Screen.width) * screenWidthInUnits;

        Vector2 paddlePosition = new Vector2(mousePositionInUnits + cameraOffset, transform.position.y);

        float paddleOffset = paddleWidth / 2;
        float minXposition = min + cameraOffset + paddleOffset;
        float maxXposition = max + cameraOffset - paddleOffset;

        paddlePosition.x = Mathf.Clamp(paddlePosition.x, minXposition, maxXposition);
        transform.position = paddlePosition;
    }
}
