﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VaseSoundController : MonoBehaviour
{
    [SerializeField] AudioClip[] audioClips;

    AudioSource audioSource;

    private void Awake() {
        audioSource = GetComponent<AudioSource>();
    }
}
