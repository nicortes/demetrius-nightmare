﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vase : MonoBehaviour
{
    [SerializeField] AudioClip[] audioClips;
    
    AudioSource audioSource;
    AudioClip randomSound;

    Level level;
    ScoreController scoreController;

    int randomIndex;
    float volume;
    Vector3 cameraPosition;
    Action OnVaseDestroyed;

    private void Awake() {
        audioSource = GetComponent<AudioSource>();
        level = FindObjectOfType<Level>();
        scoreController = FindObjectOfType<ScoreController>();
    }

    private void Start() {
        level.CountBreakableVase();
        OnVaseDestroyed += level.DiscountBreakableVase;
        OnVaseDestroyed += scoreController.increaseScore;
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        randomIndex = UnityEngine.Random.Range(0, audioClips.Length);
        randomSound = audioClips[randomIndex];
        cameraPosition = Camera.main.transform.position;
        volume = 0.1f;

        AudioSource.PlayClipAtPoint(randomSound, cameraPosition, volume);

        OnVaseDestroyed?.Invoke();
        OnVaseDestroyed -= level.DiscountBreakableVase;
        OnVaseDestroyed -= scoreController.increaseScore;
        Destroy(gameObject);
    }
}
