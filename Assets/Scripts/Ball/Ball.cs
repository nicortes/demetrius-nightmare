﻿using System;
using UnityEngine;

public class Ball : MonoBehaviour
{
    [SerializeField] Transform paddleTransform;
    [SerializeField] float ballToPaddleOffset;

    [SerializeField] float verticalLaunchSpeed;
    [SerializeField] float horizontalLaunchSpeed;

    public bool ballIsAttachedToPaddle = true;
    Rigidbody2D ballRigidBody2D;
    BallSoundController ballSoundController;
    Level level;

    private void Awake() {
        ballRigidBody2D = GetComponent<Rigidbody2D>();
        ballSoundController = GetComponent<BallSoundController>();
        level = FindObjectOfType<Level>();
    }

    // Update is called once per frame
    void Update()
    {
        if (ballIsAttachedToPaddle) {
            LockToPaddle();
            LaunchOnClick();
        }
        if (level.vasesLeft == 0) {
            this.gameObject.SetActive(false);
        }
    }

    private void LaunchOnClick() {
        if (Input.GetMouseButtonDown(0)) {
            
            ballIsAttachedToPaddle = false;
            ballRigidBody2D.velocity = new Vector2(horizontalLaunchSpeed, verticalLaunchSpeed);
        }
    }

    private void LockToPaddle() {
        transform.position = new Vector2(paddleTransform.position.x, paddleTransform.position.y + ballToPaddleOffset);
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.tag != "Vase" && !ballIsAttachedToPaddle) {
            ballSoundController.PlayBallSound();
        }
    }
}
