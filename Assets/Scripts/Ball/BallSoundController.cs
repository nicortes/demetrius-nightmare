﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallSoundController : MonoBehaviour
{
    AudioSource audioSource;
    //Ball ball;

    private void Awake() {
        audioSource = GetComponent<AudioSource>();
        //ball = GetComponent<Ball>();
    }

    public void PlayBallSound() {
            audioSource.PlayOneShot(audioSource.clip);
    }
}
